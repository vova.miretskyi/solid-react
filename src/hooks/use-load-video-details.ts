import { useEffect, useState } from "react";

export type VideoDetails = {
  previewUrl: string;
  title: string;
  author: string;
};

export type StreamDetails = VideoDetails & { watching: number };

const loadVideoDetails = (id: string): Promise<VideoDetails> => {
  return new Promise((resolve) =>
    setTimeout(
      () =>
        resolve({
          previewUrl: "https://i.ytimg.com/vi/BlNwQdqdRig/hqdefault.jpg",
          title: "функціональний Typescript: функція curry",
          author: "@AleksandrSugak",
        }),
      500
    )
  );
};

const loadStreamDetails = (id: string): Promise<StreamDetails> => {
  return new Promise((resolve) =>
    setTimeout(
      () =>
        resolve({
          previewUrl: "https://i.ytimg.com/vi/BlNwQdqdRig/hqdefault.jpg",
          title: "SOLID на практиці - потрібен чи ні?",
          author: "@AleksandrSugak",
          watching: 12000,
        }),
      500
    )
  );
};

type Params = {
  videoId: string;
};

export const useLoadVideoDetails = ({ videoId }: Params) => {
  const [videoDetails, setVideoDetails] = useState<VideoDetails>();

  useEffect(() => {
    loadVideoDetails(videoId).then((vd) => setVideoDetails(vd));
  }, [videoId]);

  return videoDetails;
};

export const useLoadStreamDetails = ({ videoId }: Params) => {
  const [streamDetails, setStreamDetails] = useState<StreamDetails>();

  useEffect(() => {
    loadStreamDetails(videoId).then((vd) => setStreamDetails(vd));
  }, [videoId]);

  return streamDetails;
};
