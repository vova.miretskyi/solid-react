import { VideoDetails } from "../../../hooks/use-load-video-details";

import { Loader } from "../../common";
import { VideoDescription } from "./DescriptionComponents";

type VideoPreviewProps = Pick<VideoDetails, "previewUrl">;

const VideoPreviewImage = ({ previewUrl }: VideoPreviewProps) => {
  return (
    <img
      style={{ width: "200px", borderRadius: "10px", border: "1px solid" }}
      src={previewUrl}
      alt=""
    />
  );
};

type Props<T extends VideoDetails> = {
  videoDetails: T;
  renderImagePreview?: (video: T) => React.ReactElement;
  renderDescription?: (video: T) => React.ReactElement;
};

export const VideoPreviewTemplate = <T extends VideoDetails>({
  videoDetails,
  renderImagePreview = (video) => (
    <VideoPreviewImage previewUrl={video.previewUrl} />
  ),
  renderDescription = ({ previewUrl, ...rest }) => (
    <VideoDescription {...rest} />
  ),
}: Props<T>) => {
  return (
    <div style={{ display: "flex" }}>
      {renderImagePreview(videoDetails)}
      <div style={{ paddingLeft: "10px" }}>
        {renderDescription(videoDetails)}
      </div>
    </div>
  );
};

export const SelfLoadingVideoPreview = <T extends VideoDetails>({
  videoId,
  useVideoDetails,
  renderVideoPreview = (video) => <VideoPreviewTemplate videoDetails={video} />,
  LoaderComponent = Loader,
}: {
  videoId: string;
  useVideoDetails: ({ videoId }: { videoId: string }) => T | undefined;
  renderVideoPreview?: (video: T) => React.ReactElement;
  LoaderComponent?: React.FunctionComponent<{}>;
}) => {
  const videoDetails = useVideoDetails({ videoId });

  return videoDetails ? renderVideoPreview(videoDetails) : <LoaderComponent />;
};
