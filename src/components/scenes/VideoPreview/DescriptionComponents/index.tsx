import {
  StreamDetails,
  VideoDetails,
} from "../../../../hooks/use-load-video-details";

type VideoDescriptionProps = Pick<VideoDetails, "title" | "author">;

export const VideoDescription = ({ title, author }: VideoDescriptionProps) => {
  return (
    <>
      <div style={{ fontWeight: "bold" }}>{title}</div>
      <div style={{ color: "#808080" }}>{author}</div>
    </>
  );
};

type StreamDescriptionProps = Pick<
  StreamDetails,
  "author" | "title" | "watching"
>;

export const StreamDescription = ({
  watching,
  ...rest
}: StreamDescriptionProps) => {
  return (
    <>
      <VideoDescription {...rest} />
      <div style={{ color: "#808080" }}>{watching}</div>
      <span
        style={{
          color: "white",
          backgroundColor: "red",
          padding: "3px",
          marginTop: "5px",
          display: "block",
          width: "fit-content",
        }}
      >
        live
      </span>
    </>
  );
};
