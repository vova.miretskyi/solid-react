import { SelfLoadingVideoPreview, VideoPreviewTemplate } from "../VideoPreview";
import {
  useLoadStreamDetails,
  useLoadVideoDetails,
} from "../../../hooks/use-load-video-details";
import { StreamDescription } from "../VideoPreview/DescriptionComponents";

export const Main = () => {
  return (
    <div>
      <SelfLoadingVideoPreview
        videoId={"testVideo"}
        useVideoDetails={useLoadVideoDetails}
      />
      <br />
      <SelfLoadingVideoPreview
        videoId={"streamVideo"}
        useVideoDetails={useLoadStreamDetails}
        renderVideoPreview={(video) => (
          <VideoPreviewTemplate
            videoDetails={video}
            renderDescription={({ previewUrl, ...rest }) => (
              <StreamDescription {...rest} />
            )}
          />
        )}
      />
    </div>
  );
};
